//Créer une fonction qui retourn le nombre de voyelles (excepté y) dans une string 
// 1- Ma méthode 
function getCount(str) {
    let count = 0
        for (let i = 0; i < str.length; i++) { 
         if (str.charAt(i) === "a" || str.charAt(i) === "e" || str.charAt(i) === "i" || str.charAt(i) === "o" || str.charAt(i) === "u") {
           count ++
         }
       }
     return count;
   }

   //Autres méthodes
   function countVowels(str) {    
    const vowels = 'aeiouAEIOU';
    let count = 0;
    for (let char of str) {
        if (vowels.includes(char)) {
            count++;
        }
    }
    
    return count;
}


//Make a program that filters a list of strings and returns a list with only your friends name in it. If a name has exactly 4 letters in it, you can be sure that it has to be a friend of yours! Otherwise, you can be sure he's not...

function friend(friends){
    let myfriends = []
    for (let i = 0; i <= friends.length; i++){      
      if(friends[i].length === 4){
        myfriends.push(friends[i])
      }             
    }   
   return myfriends /*Le résultat doit être à l'extérieur de la boucle dans les deux solutions sinon ça ne marche pas.*/ 
}

function friend(friends){
    let myfriends = []
    for (let i of friends) {        
        if(i.length === 4){
            myfriends.push(i)
        }      
      }
      return myfriends
   }

   
   //In a factory a printer prints labels for boxes. For one kind of boxes the printer has to use colors which, for the sake of simplicity, are named with letters from a to m. The colors used by the printer are recorded in a control string. For example a "good" control string would be aaabbbbhaijjjm meaning that the printer used three times color a, four times color b, one time color h then one time color a... Sometimes there are problems: lack of colors, technical malfunction and a "bad" control string is produced e.g. aaaxbbbbyyhwawiwjjjwwm with letters not from a to m. You have to write a function printer_error which given a string will return the error rate of the printer as a string representing a rational whose numerator is the number of errors and the denominator the length of the control string. Don't reduce this fraction to a simpler expression. The string has a length greater or equal to one and contains only letters from ato z.
   function printerError(s) {
    let errors = []
    for (i of s){
      if (('nopqrstuvwxyz').includes(i)){
        errors.push(i)
      }
    }
  return `${errors.length}/${s.length}`
}
//2ème méthode 
function printerError(s) {
    // your code
    var count = 0;
    for(var i = 0; i < s.length; i++) {
      if (s[i] > "m") {
        count++;
      }
    }
    return count+"/"+s.length;
}
//3 méthode
/*const printerError = s => `${s.replace(/[a-m]/gi, "").length}/${s.length}`;*/


//fonction triangle
function isTriangle(a,b,c)
{
   return a + b > c && a + c > b && c + b > a;
}
//2ème méthode
var isTriangle = (a,b,c) => Math.max(a,b,c)<(a+b+c)/2
 //3ème méthode
 function isTriangle(a,b,c) {
    var sides = [a, b, c].sort();
    return (sides[0] + sides[1] > sides[2]);
  }


//Write a function that takes in a string of one or more words, and returns the same string, but with all words that have five or more letters reversed (Just like the name of this Kata). Strings passed in will consist of only letters and spaces. Spaces will be included only when more than one word is present.
function spinWords(string){
  let str = string.split(' ')
  for (i = 0; i<str.length; i++){
    if (str[i].length >= 5) {
      str[i] = str[i].split('').reverse().join('')
    }
  }
  return str.join(" ")
}


//You probably know the "like" system from Facebook and other pages. People can "like" blog posts, pictures or other items. We want to create the text that should be displayed next to such an item. Implement the function which takes an array containing the names of people that like an item.
function likes(names) {
  if (names.length === 0){
    return 'no one likes this'
  }
  if (names.length === 1){
    return `${names[0]} likes this`
  }
  if (names.length === 2){
    return `${names[0]} and ${names[1]} like this`
  }
  if (names.length === 3){
    return `${names[0]}, ${names[1]} and ${names[2]} like this`
  }
  if (names.length >= 4){
    let restlength = 0
    for (let i = 2; i<names.length; i++){
      restlength++      
    }   
    return `${names[0]}, ${names[1]} and ${restlength} others like this`
  }
}
//Autre magnifique méthode
{
  function likes(names) {
    names = names || [];
    switch(names.length){
      case 0: return 'no one likes this'; break;
      case 1: return names[0] + ' likes this'; break;
      case 2: return names[0] + ' and ' + names[1] + ' like this'; break;
      case 3: return names[0] + ', ' + names[1] + ' and ' + names[2] + ' like this'; break;
      default: return names[0] + ', ' + names[1] + ' and ' + (names.length - 2) + ' others like this';
    }
  }
}


//Générateur de #hashtag
let str = 'jfsnf f osfkoskf kf'
  function generateHashtag (str) {
    let string = str.split(' ')
    let string1 = string.map(element => element.charAt(0).toUpperCase() + element.slice(1)).join('')
    if (string1 && string1.length < 140) {
      return `#${string1}`
    }
    if (string1.length == "0"){
      return false
    } 
    if (string1.length >= 140) {
        return false
    }
  }

/*console.log(generateHashtag (str))*/
//Autre méthode
function generateHashtag(s) {
  s = s.replace(/[a-zA-Z]+/g, word=>word[0].toUpperCase() + word.substring(1).toLowerCase())
  s = s.replace(/\s+/g, '')

  if (s.length == 0 || s.length > 139)
      return false

  return '#' + s
}
//2ème autre méthode
function generateHashtag (str) {

  var hashtag = str.split(' ').reduce(function(tag, word) {
    return tag + word.charAt(0).toUpperCase() + word.substring(1);
  }, '#');
  
  return hashtag.length == 1 || hashtag.length > 140 ? false : hashtag;
}


// Maximum subarray sum : Voir array.reduce
let arr = [-1, 2 ,3 ,-23, 23 ,2]
var maxSequence = function(arr){ 
  let arrsum = 0
  for (let i= 0; i<arr.length; i++ ){
     if (arr === []){
      arrsum = arr.reduce((a, b) => a + b, 0)     
  }  
  else {
     arrsum = arr.reduce((a, b) => a + b, 5)
    }    
  }
  return arrsum
}

console.log(maxSequence(arr))


//Consecutive strings
let strarr = ["zone", "abigail", "theta", "form", "libe", "zas",]
let k = 2
function longestConsec(strarr, k) {
  let chain = []
  /*for (let i = 0; i<strarr.length; i++){
          for(let j = 1; j<strarr.length; j++){
            if (strarr[i] != strarr[j]) {
              chain.push(strarr[i] + strarr[j])
            }            
          }          
      }*/
      for (let i = 0; i<strarr.length; i++){
        chain.push(strarr[i] + strarr[k])
        
      }
      return strarr[k]       
      /*let chainmax = []
      for (let i = 0; i<chain.length; i++){
        for(let j = 1; j<chain.length; j++ ){          
          if (chain[i].length < chain[j].length){
            chainmax.push(chain[j])                                           
          }                                            
        }
        chainmax.pop()
        return chainmax.toString()
      }*/
      
}
console.log(longestConsec(strarr))